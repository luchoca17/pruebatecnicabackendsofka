package com.sofka.demo.appUtils;


import com.sofka.demo.dto.CyclistDTO;
import com.sofka.demo.dto.CyclistTeamDTO;
import com.sofka.demo.schema.Cyclist;
import com.sofka.demo.schema.CyclistTeam;
import org.springframework.beans.BeanUtils;

public class AppUtils {

    //PASAR DE SCHEMA A DTO

    public static CyclistDTO cyclistToDTO (Cyclist cyclist){
        CyclistDTO cyclistDTO = new CyclistDTO();
        BeanUtils.copyProperties(cyclist,cyclistDTO);
        return cyclistDTO;
    }

    public static CyclistTeamDTO cyclistTeamToDTO(CyclistTeam cyclistTeam){
        CyclistTeamDTO cyclistTeamDTO = new CyclistTeamDTO();
        BeanUtils.copyProperties(cyclistTeam,cyclistTeamDTO);
        return cyclistTeamDTO;
    }

    //DE DTO A SCHEMA

    public static Cyclist DTOtoCyclist(CyclistDTO cyclistDTO){
        Cyclist cyclist = new Cyclist();
        BeanUtils.copyProperties(cyclistDTO,cyclist);
        return cyclist;
    }


    public static CyclistTeam DTOtoCyclistTeam (CyclistTeamDTO cyclistTeamDTO){
        CyclistTeam cyclistTeam = new CyclistTeam();
        BeanUtils.copyProperties(cyclistTeamDTO,cyclistTeam);
        return cyclistTeam;
    }
}
