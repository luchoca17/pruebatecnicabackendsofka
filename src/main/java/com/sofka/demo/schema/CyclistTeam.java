package com.sofka.demo.schema;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "team")
@AllArgsConstructor
@NoArgsConstructor
public class CyclistTeam {
    @Id
    private String id;

    private String name;

    private String teamIdentify;

    private String country;

   private List<String> cyclists;


}
