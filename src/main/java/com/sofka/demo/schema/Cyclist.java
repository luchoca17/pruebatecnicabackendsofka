package com.sofka.demo.schema;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "cyclist")
@AllArgsConstructor
@NoArgsConstructor
public class Cyclist {

        @Id
        private String id;

        private String name;

        private String identifyNumber;

        private String nacionality;

        private String team;
    }
