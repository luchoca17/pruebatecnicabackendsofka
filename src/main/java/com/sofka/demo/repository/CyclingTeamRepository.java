package com.sofka.demo.repository;


import com.sofka.demo.schema.CyclistTeam;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CyclingTeamRepository extends ReactiveMongoRepository<CyclistTeam,String> {
}
