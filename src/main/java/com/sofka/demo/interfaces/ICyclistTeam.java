package com.sofka.demo.interfaces;

import com.sofka.demo.dto.CyclistDTO;
import com.sofka.demo.dto.CyclistTeamDTO;
import com.sofka.demo.schema.CyclistTeam;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICyclistTeam {

    Flux<CyclistTeamDTO> findAllTeams();
    Mono<CyclistTeamDTO> findTeamById(String cyclistTeamId);
    Mono<CyclistTeamDTO> saveTeam(CyclistTeamDTO cyclistTeamDTO);
    Mono<CyclistTeamDTO> updateTeam(CyclistTeamDTO cyclistTeamDTO, String cyclistTeamId);
    Flux<CyclistTeamDTO> getTeamsByCountry(String country);
    Flux<CyclistTeamDTO> getTeamByIdentifyer(String identifyer);
    Mono<Void> deleteTeam (String cyclistTeamId);
    Mono<CyclistTeamDTO> addCyclistToTeam(String cyclistTeamId,String cyclistId);
}
