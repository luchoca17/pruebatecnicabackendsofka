package com.sofka.demo.interfaces;

import com.sofka.demo.dto.CyclistDTO;
import com.sofka.demo.schema.Cyclist;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICyclist {

    Flux<CyclistDTO> findAllCyclists();
    Mono<CyclistDTO> findCyclistById(String cyclistId);
    Mono<CyclistDTO> saveCyclist(CyclistDTO cyclistDTO);
    Mono<CyclistDTO> updateCyclist(CyclistDTO cyclistDTO, String cyclistId);
    Flux<CyclistDTO> findCyclistsByNacionality(String nacionality);
    Mono<Void> deleteCyclist (String cyclistId);

    Mono<CyclistDTO> getCyclistByTeamIdentifyer(Cyclist cyclist);
}
