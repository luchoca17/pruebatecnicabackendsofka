package com.sofka.demo.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CyclistDTO {

    private String name;

    private String identifyNumber;

    private String nacionality;

    private String team;
}
