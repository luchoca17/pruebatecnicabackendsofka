package com.sofka.demo.dto;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CyclistTeamDTO {

    private String name;

    private String teamIdentify;

    private String country;

    private List<String> cyclists;
}
