package com.sofka.demo.services;

import com.sofka.demo.appUtils.AppUtils;
import com.sofka.demo.dto.CyclistTeamDTO;
import com.sofka.demo.interfaces.ICyclistTeam;
import com.sofka.demo.repository.CyclingTeamRepository;
import com.sofka.demo.schema.CyclistTeam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CyclistTeamService implements ICyclistTeam {

    @Autowired
    CyclingTeamRepository cyclingTeamRepository;

    @Override
    public Flux<CyclistTeamDTO> findAllTeams() {
        return cyclingTeamRepository.findAll().map(AppUtils::cyclistTeamToDTO);
    }

    @Override
    public Mono<CyclistTeamDTO> findTeamById(String cyclistTeamId) {
        return cyclingTeamRepository.findById(cyclistTeamId).map(AppUtils::cyclistTeamToDTO);
    }

    @Override
    public Mono<CyclistTeamDTO> saveTeam(CyclistTeamDTO cyclistTeamDTO) {
        CyclistTeam cyclistTeam = AppUtils.DTOtoCyclistTeam(cyclistTeamDTO);
        return cyclingTeamRepository.save(cyclistTeam)
                .map(AppUtils::cyclistTeamToDTO);
    }

    @Override
    public Mono<CyclistTeamDTO> updateTeam(CyclistTeamDTO cyclistTeamDTO, String cyclistTeamId) {
        return cyclingTeamRepository.findById(cyclistTeamId)
                .flatMap(cyclistTeam -> {
                    CyclistTeam cyclistTeam1 = AppUtils.DTOtoCyclistTeam(cyclistTeamDTO);
                    cyclistTeam1.setId(cyclistTeam.getId());
                    return cyclingTeamRepository.save(cyclistTeam1);
                }).map(AppUtils::cyclistTeamToDTO);
    }

    @Override
    public Flux<CyclistTeamDTO> getTeamsByCountry(String country) {
        return cyclingTeamRepository.findAll().filter(cyclistTeam ->
                cyclistTeam.getCountry().equals(country)).map(AppUtils::cyclistTeamToDTO);
    }

    @Override
    public Flux<CyclistTeamDTO> getTeamByIdentifyer(String identifyer) {
        return cyclingTeamRepository.findAll().filter(cyclistTeam -> cyclistTeam.getTeamIdentify().equals(identifyer))
                .map(AppUtils::cyclistTeamToDTO);
    }

    @Override
    public Mono<Void> deleteTeam(String cyclistTeamId) {
        return cyclingTeamRepository.findById(cyclistTeamId).flatMap(cyclistTeam -> cyclingTeamRepository
                        .deleteById(String.valueOf(cyclistTeam.getId())))
                .switchIfEmpty(Mono.empty());
    }

    @Override
    public Mono<CyclistTeamDTO> addCyclistToTeam(String cyclistTeamId, String cyclistId) {
        return null;
    }
}