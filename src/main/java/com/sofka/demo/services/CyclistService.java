package com.sofka.demo.services;

import com.sofka.demo.appUtils.AppUtils;
import com.sofka.demo.dto.CyclistDTO;
import com.sofka.demo.interfaces.ICyclist;
import com.sofka.demo.repository.CyclistRepository;
import com.sofka.demo.schema.Cyclist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class CyclistService implements ICyclist {

    @Autowired
    CyclistRepository cyclistRepository;


    //CREAR CICLISTA
    @Override
    public Mono<CyclistDTO> saveCyclist(CyclistDTO cyclistDTO) {
        Cyclist cyclist = AppUtils.DTOtoCyclist(cyclistDTO);
        return cyclistRepository.save(cyclist).map(AppUtils::cyclistToDTO);
    }

    //BUSCAR TODOS LOS CICLISTAS
    @Override
    public Flux<CyclistDTO> findAllCyclists() {
        return cyclistRepository.findAll().map(AppUtils::cyclistToDTO);
    }

    //BUSCAR CICLISTA POR ID
    @Override
    public Mono<CyclistDTO> findCyclistById(String cyclistId) {
        return cyclistRepository.findById(cyclistId).map(AppUtils::cyclistToDTO);
    }

    //ACTUALIZAR CICLISTAS
    @Override
    public Mono<CyclistDTO> updateCyclist(CyclistDTO cyclistDTO, String cyclistId) {

        return cyclistRepository.findById(cyclistId).flatMap(cyclist ->{
                    Cyclist cyclist1 = AppUtils.DTOtoCyclist(cyclistDTO);
                    cyclist1.setId(cyclistId);
                    return cyclistRepository.save(cyclist1);
                }).map(AppUtils::cyclistToDTO).switchIfEmpty(Mono.empty());
    }


    //BUSCAR CICLISTAS POR PAIS/NACIONALIDAD
    @Override
    public Flux<CyclistDTO> findCyclistsByNacionality(String nacionality) {
        return cyclistRepository.findAll().filter(cyclist ->
                cyclist.getNacionality().equals(nacionality)).map(AppUtils::cyclistToDTO);
    }

    //ELIMINAR CICLISTA POR ID
    @Override
    public Mono<Void> deleteCyclist(String cyclistId) {
        return cyclistRepository
                .findById(cyclistId)
                .flatMap(cyclist -> cyclistRepository.deleteById(cyclist.getId()))
                .switchIfEmpty(Mono.empty());
    }

    ///BUSCAR CICLISTA POR EL IDENTIFICADOR DE EQUIPO(NUMERO)
    @Override
    public Mono<CyclistDTO> getCyclistByTeamIdentifyer(Cyclist cyclist) {
        return null;
//        return cyclistRepository.getCyclistByTeamIdentifyer(teamIdentify).map(AppUtils::cyclistToDTO);
    }

}
