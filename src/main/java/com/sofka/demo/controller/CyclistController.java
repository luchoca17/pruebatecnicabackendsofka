package com.sofka.demo.controller;


import com.sofka.demo.dto.CyclistDTO;
import com.sofka.demo.services.CyclistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/cyclist")
public class CyclistController {

    @Autowired
    private CyclistService cyclistService;

    @PostMapping("/saveCyclist")
    public ResponseEntity<Mono<CyclistDTO>> saveCyclist(@RequestBody CyclistDTO cyclistDTO){
        return ResponseEntity.ok().body(cyclistService.saveCyclist(cyclistDTO));
    }

    @GetMapping("/findAllCyclists")
    public  ResponseEntity<Flux<CyclistDTO>> findAllCyclists(){
        return ResponseEntity.ok().body(cyclistService.findAllCyclists());
    }

    @GetMapping("/findCyclistById/{cyclistId}")
    public Mono<ResponseEntity<CyclistDTO>> findCyclistById(@PathVariable("cyclistId") String cyclistId){
        return cyclistService.findCyclistById(cyclistId).map(x -> ResponseEntity.ok().body(x))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/delete/{cyclistId}")
    public Mono<ResponseEntity<Void>> deleteCyclist(@PathVariable("cyclistId")String cyclistId){
        return cyclistService.deleteCyclist(cyclistId).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/updateCyclist/{cyclistId}")
    public ResponseEntity<Mono<CyclistDTO>> updateCyclist(@RequestBody CyclistDTO cyclistDTO, @PathVariable String cyclistId){
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                .body(cyclistService.updateCyclist(cyclistDTO,cyclistId));
    }

    @GetMapping("/findCyclistByNacionality/{nacionality}")
    public ResponseEntity<Flux<CyclistDTO>> findCyclistByNacionality(@PathVariable(value = "nacionality") String nacionality){
        return ResponseEntity.ok().body(cyclistService.findCyclistsByNacionality(nacionality));
    }

//    Mono<CyclistDTO> getCyclistByTeamIdentifyer(Cyclist cyclist);



}
