package com.sofka.demo.controller;


import com.sofka.demo.dto.CyclistTeamDTO;
import com.sofka.demo.services.CyclistTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/team")
public class CyclingTeamController {

    @Autowired
    CyclistTeamService cyclistTeamService;
    @PostMapping("/saveTeam")
    public ResponseEntity<Mono<CyclistTeamDTO>> saveTeam(@RequestBody CyclistTeamDTO cyclistTeamDTO){
        System.out.println(cyclistTeamDTO);
        return ResponseEntity.ok().body(cyclistTeamService.saveTeam(cyclistTeamDTO));
    }

    @GetMapping("/findAllTeams")
    public ResponseEntity<Flux<CyclistTeamDTO>> findAllCyclingTeam(){
        return ResponseEntity.ok().body(cyclistTeamService.findAllTeams());
    }

    @GetMapping("/findTeamById/{cyclistTeamId}")
    public Mono<ResponseEntity<CyclistTeamDTO>> findTeamById(@PathVariable ("cyclistTeamId")String cyclistTeamId){
        return cyclistTeamService.findTeamById(cyclistTeamId)
                .map(x -> ResponseEntity.ok().body(x))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/updateTeam/{cyclistTeamId}")
    public ResponseEntity<Mono<CyclistTeamDTO>> updateTeam(@RequestBody CyclistTeamDTO cyclistTeamDTO, @PathVariable ("cyclistTeamId")String cyclistTeamId){
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                .body(cyclistTeamService.updateTeam(cyclistTeamDTO, cyclistTeamId));
    }

    @DeleteMapping("/deleteTeam/{cyclistTeamId}")
    public Mono<ResponseEntity<Void>> deleteTeam(@PathVariable("cyclistTeamId")String cyclistTeamId){
        return cyclistTeamService.deleteTeam(cyclistTeamId).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping("/getTeamsByCountry/{country}")
    public ResponseEntity<Flux<CyclistTeamDTO>> getTeamsByCountry(@PathVariable (value = "country")String country){
        return ResponseEntity.ok().body(cyclistTeamService.getTeamsByCountry(country));
    }

    @GetMapping("/getTeamsByIdentifyer/{identifyer}")
    public ResponseEntity<Flux<CyclistTeamDTO>> getTeamsByIdentifyer(@PathVariable (value = "identifyer")String identifyer){
        return ResponseEntity.ok().body(cyclistTeamService.getTeamByIdentifyer(identifyer));
    }

    @PatchMapping("/addCyclistToTeam/{cyclistTeamId}/{cyclistId}")
    public Mono<CyclistTeamDTO> addCyclistToTeam(@PathVariable("cyclistTeamId")String cyclistTeamId,
                                                 @PathVariable("cyclistId")String cyclistId){
        return cyclistTeamService.addCyclistToTeam(cyclistTeamId,cyclistId);
    }



}
